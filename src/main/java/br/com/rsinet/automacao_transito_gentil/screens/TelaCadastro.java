package br.com.rsinet.automacao_transito_gentil.screens;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import br.com.rsinet.automacao_transito_gentil.screens.widget.AbstractInputArea;
import br.com.rsinet.automacao_transito_gentil.screens.widget.AndroidInputArea;
import br.com.rsinet.automacao_transito_gentil.screens.widget.iOSInputArea;
import br.com.rsinet.automacao_transito_gentil.util.CpfUtil;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.OverrideWidget;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class TelaCadastro {

	private WebDriver driver;

	private Wait<WebDriver> wait;

	private String cpf;

	public TelaCadastro(WebDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		this.driver = driver;
		wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(15))
				.pollingEvery(Duration.ofMillis(1000))
				.ignoring(NoSuchElementException.class, org.openqa.selenium.WebDriverException.class);
		this.cpf = CpfUtil.geraCPF();
	}

	@AndroidFindBy(xpath = "//android.widget.EditText[1]")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTextField")
	@OverrideWidget(androidUIAutomator = AndroidInputArea.class, iOSXCUITAutomation = iOSInputArea.class)
	private AbstractInputArea edtEmail;

	@AndroidFindBy(xpath = "//android.view.View/android.view.View[3]/android.widget.EditText")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeSecureTextField")
	@OverrideWidget(androidUIAutomator = AndroidInputArea.class, iOSXCUITAutomation = iOSInputArea.class)
	private AbstractInputArea edtSenha;

	@AndroidFindBy(xpath = "//android.view.View/android.view.View[4]/android.widget.EditText")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeSecureTextField")
	@OverrideWidget(androidUIAutomator = AndroidInputArea.class, iOSXCUITAutomation = iOSInputArea.class)
	private AbstractInputArea edtConfirmarSenha;

	@AndroidFindBy(uiAutomator = "new UiSelector().textMatches(\".*Cadastar.*\")")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[6]/XCUIElementTypeButton")
	private MobileElement btnCadastrar;

	@AndroidFindBy(xpath = "//*[@resource-id='textActivationInsurer']")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeTextField")
	@OverrideWidget(androidUIAutomator = AndroidInputArea.class, iOSXCUITAutomation = iOSInputArea.class)
	private AbstractInputArea edtCpf;

	@AndroidFindBy(xpath = "//*[@resource-id='textDateOfBirth']")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeTextField")
	@OverrideWidget(androidUIAutomator = AndroidInputArea.class, iOSXCUITAutomation = iOSInputArea.class)
	private AbstractInputArea edtDataNascimento;

	@AndroidFindBy(xpath = "//*[@resource-id='ltp-submit']")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeButton")
	private MobileElement btnEntrar;

	@AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"OK\")")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='OK']")
	private MobileElement btnOk;

	@AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"Aceitar e Continuar\")")
	@iOSFindBy(accessibility = "Aceitar e Continuar")
	private MobileElement btnAceitarContinuar;

	@AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"Pular\")")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeButton")
	private MobileElement btnPular;

	public AbstractInputArea getEdtEmail() {
		return edtEmail;
	}

	public AbstractInputArea getEdtSenha() {
		return edtSenha;
	}

	public AbstractInputArea getEdtConfirmarSenha() {
		return edtConfirmarSenha;
	}

	public MobileElement getBtnCadastrar() {
		return btnCadastrar;
	}

	public AbstractInputArea getEdtCpf() {
		return edtCpf;
	}

	public AbstractInputArea getEdtDataNascimento() {
		return edtDataNascimento;
	}

	public MobileElement getBtnEntrar() {
		return this.btnEntrar;
	}

	public MobileElement getBtnAceitarContinuar() {
		return this.btnAceitarContinuar;
	}

	public MobileElement getBtnPular() {
		return this.btnPular;
	}

	public MobileElement getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(MobileElement btnOk) {
		this.btnOk = btnOk;
	}

	public void digitarEmail(String email) {
		wait.until(ExpectedConditions.visibilityOf(getEdtEmail().getWrappedElement()));
		String[] splitedEmail = email.split("@");
		email = String.format("%s%s@%s", splitedEmail[0], this.cpf, splitedEmail[1]);
		getEdtEmail().sendKeys(email);
	}

	public void digitarSenha(String senha) {
		getEdtSenha().sendKeys(senha);
		getEdtConfirmarSenha().sendKeys(senha);
	}

	public void digitarConfirmarSenha(String confirmarSenha) {
		getEdtConfirmarSenha().sendKeys(confirmarSenha);
	}

	public void clicarEmCadastrar() {
		wait.until(ExpectedConditions.elementToBeClickable(getBtnCadastrar()));
		getBtnCadastrar().click();
	}

	public void digitarCpf() {
		wait.until(ExpectedConditions.visibilityOf(getBtnEntrar()));
		this.getEdtCpf().sendKeys(this.cpf);
	}

	public void digitarDataNascimento(String dataNascimento) {
		this.getEdtDataNascimento().sendKeys(dataNascimento);
	}

	public void clicarEmEntrar() {
		getBtnEntrar().click();
	}

	public void clicarEmOk() {
		wait.until(ExpectedConditions.elementToBeClickable(getBtnOk()));
		getBtnOk().click();
	}

	public void clicarAceitarContinuar() {
		wait.until(ExpectedConditions.elementToBeClickable(getBtnAceitarContinuar()));
		getBtnAceitarContinuar().click();
	}

	public void clicarEmPular() {
		if (((AppiumDriver) driver).getCapabilities().getPlatform().toString().equalsIgnoreCase("ios"))
			try {
				driver.findElement(By.xpath("//XCUIElementTypeButton[@name='OK']")).click();
			} catch (Exception e) {

			}
		wait.until(ExpectedConditions.elementToBeClickable(getBtnPular()));
		getBtnPular().click();
	}

	public void verticalSwipeUp() {
		double startPercentage = 0.55;
		double endPercentage = 0.15;
		double anchorPercentage = 0.50;
		Dimension size = driver.manage().window().getSize();
		int middleX = (int) (size.width * 0.5);
		int middleY = (int) (size.height * 0.5);
		int anchor = (int) (size.width * anchorPercentage);
		int startPoint = (int) (size.height * startPercentage);
		int endPoint = (int) (size.height * endPercentage);

		new TouchAction((AppiumDriver) driver).press(PointOption.point(anchor, startPoint))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(1500)))
				.moveTo(PointOption.point(anchor, endPoint)).release().perform();
	}

	public void verticalSwipeDown() {
		double endPercentage = 0.75;
		double startPercentage = 0.15;
		double anchorPercentage = 0.50;
		Dimension size = driver.manage().window().getSize();
		int middleX = (int) (size.width * 0.5);
		int middleY = (int) (size.height * 0.5);
		int anchor = (int) (size.width * anchorPercentage);
		int startPoint = (int) (size.height * startPercentage);
		int endPoint = (int) (size.height * endPercentage);

		new TouchAction((AppiumDriver) driver).press(PointOption.point(anchor, startPoint))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(1500)))
				.moveTo(PointOption.point(anchor, endPoint)).release().perform();
	}
}
