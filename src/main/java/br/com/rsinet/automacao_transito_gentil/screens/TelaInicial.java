package br.com.rsinet.automacao_transito_gentil.screens;

import java.time.Duration;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class TelaInicial {

	Wait<WebDriver> wait;

	public TelaInicial(WebDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(15))
				.pollingEvery(Duration.ofMillis(1000))
				.ignoring(NoSuchElementException.class, org.openqa.selenium.WebDriverException.class);
	}

	@AndroidFindBy(xpath = "//android.view.View[@resource-id='login-content']/android.view.View/android.widget.Button[1]")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeButton[1]")
	private MobileElement btnToLogin;

	@AndroidFindBy(xpath = "//android.view.View[@resource-id='login-content']/android.view.View/android.widget.Button[2]")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeButton[2]")
	private MobileElement btnToRegister;

	public MobileElement getBtnToLogin() {
		return btnToLogin;
	}

	public MobileElement getBtnToRegister() {
		return btnToRegister;
	}

	public void clicarEmFazerLogin() {
		getBtnToLogin().click();
	}

	public void clicarEmRegistrar() {
		getBtnToRegister().click();
	}

	public void validarTelaInicial() {
		wait.until(ExpectedConditions.elementToBeClickable(getBtnToLogin()));
	}
}
