package br.com.rsinet.automacao_transito_gentil.screens;

import java.time.Duration;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import br.com.rsinet.automacao_transito_gentil.screens.widget.AbstractInputArea;
import br.com.rsinet.automacao_transito_gentil.screens.widget.AndroidInputArea;
import br.com.rsinet.automacao_transito_gentil.screens.widget.iOSInputArea;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.OverrideWidget;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class TelaLogin {

	private WebDriver driver;

	private Wait<WebDriver> wait;
	
	public TelaLogin(WebDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		this.wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(20))
				.pollingEvery(Duration.ofMillis(1000))
				.ignoring(NoSuchElementException.class, org.openqa.selenium.WebDriverException.class);
		this.driver = driver;
	}

	@AndroidFindBy(xpath = "//android.view.View[@resource-id='login-content']/android.view.View/android.view.View[2]/android.widget.EditText")
	@iOSFindBy(xpath="//XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTextField")
	@OverrideWidget(androidUIAutomator=AndroidInputArea.class, iOSXCUITAutomation=iOSInputArea.class)
	private AbstractInputArea edtEmail;

	@AndroidFindBy(xpath = "//android.view.View[@resource-id='login-content']/android.view.View/android.view.View[3]/android.widget.EditText")
	@iOSFindBy(xpath="//XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeSecureTextField")
	@OverrideWidget(androidUIAutomator=AndroidInputArea.class, iOSXCUITAutomation=iOSInputArea.class)
	private AbstractInputArea edtSenha;
	
	@AndroidFindBy(xpath = "//android.view.View[@resource-id='login-content']/android.view.View/android.widget.Button[2]")
	@iOSXCUITFindBy(iOSNsPredicate="name=\"Entrar\"")
	private MobileElement btnEntrar;
	
	@AndroidFindBy(uiAutomator="new UiSelector().className(\"android.widget.Image\")")
	@iOSXCUITFindBy(iOSNsPredicate="type='XCUIElementTypeImage'")
	private MobileElement lblEsqueceuSenha;

	public WebDriver getDriver() {
		return driver;
	}

	public AbstractInputArea getEdtEmail() {
		return edtEmail;
	}

	public AbstractInputArea getEdtSenha() {
		return edtSenha;
	}

	public MobileElement getBtnEntrar() {
		return btnEntrar;
	}
	
	public void digitarEmail(String email) {
		getEdtEmail().sendKeys(email);
	}
	
	public void digitarSenha(String senha) {
		getEdtSenha().sendKeys(senha);
	}
	
	public void clicarEmLogin() {
		getBtnEntrar().click();
	}
	
	public void validarTelaLogin() {
		wait.until(ExpectedConditions.visibilityOf(lblEsqueceuSenha));
	}
}
