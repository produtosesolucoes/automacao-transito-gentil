package br.com.rsinet.automacao_transito_gentil.screens;

import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import br.com.rsinet.automacao_transito_gentil.screens.widget.AbstractSettingsItem;
import br.com.rsinet.automacao_transito_gentil.screens.widget.AndroidSettingItem;
import br.com.rsinet.automacao_transito_gentil.screens.widget.iOSSettingItem;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.OverrideWidget;
import io.appium.java_client.pagefactory.iOSFindBy;

public class TelaPrincipal {

	Wait<WebDriver> wait;

	public TelaPrincipal(WebDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(15))
				.pollingEvery(Duration.ofMillis(1000))
				.ignoring(NoSuchElementException.class, org.openqa.selenium.WebDriverException.class);
	}

	@AndroidFindBy(xpath = "//*[@text=\"Painel\"]")
	@iOSFindBy(accessibility = "Última viagem")
	private MobileElement lblValidacao;

	@AndroidFindBy(xpath = "//android.view.View[@resource-id='menuContent']/android.view.View/android.view.View/android.view.View/android.widget.Button")
	@iOSFindBy(xpath = "//XCUIElementTypeOther[@name='Dashboard']/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton")
	private MobileElement menu;

	@AndroidFindBy(xpath = "//android.view.View[@resource-id='menu-content']/android.view.View/android.view.View[3]/android.view.View")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='']")
	@OverrideWidget(androidUIAutomator = AndroidSettingItem.class, iOSXCUITAutomation = iOSSettingItem.class)
	private AbstractSettingsItem settings;

	@AndroidFindBy(uiAutomator = "new UiSelector().text(\"Sair\")")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Sair\"]")
	private MobileElement btnSair;

	public MobileElement getValidacao() {
		return lblValidacao;
	}

	public void validarTelaPrincipal() {
		wait.until(ExpectedConditions.visibilityOf(getValidacao()));
		Assert.assertTrue(getValidacao().isDisplayed());
	}

	public void fazerLogout() {
		this.menu.click();
		this.settings.click();
		wait.until(ExpectedConditions.elementToBeClickable(this.btnSair));
		this.btnSair.click();
	}
}
