package br.com.rsinet.automacao_transito_gentil.screens.widget;

import org.openqa.selenium.WebElement;

import io.appium.java_client.pagefactory.Widget;

public abstract class AbstractInputArea extends Widget {

	public AbstractInputArea(WebElement element) {
		super(element);
	}
	
	public abstract void sendKeys(String text);

}
