package br.com.rsinet.automacao_transito_gentil.screens.widget;

import org.openqa.selenium.WebElement;

import io.appium.java_client.pagefactory.Widget;

public abstract class AbstractSettingsItem extends Widget {

	public AbstractSettingsItem(WebElement element) {
		super(element);
	}

	public abstract void click();
}
