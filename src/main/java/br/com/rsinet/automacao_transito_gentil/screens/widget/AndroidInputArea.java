package br.com.rsinet.automacao_transito_gentil.screens.widget;

import java.time.Duration;

import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class AndroidInputArea extends AbstractInputArea {

	public AndroidInputArea(WebElement element) {
		super(element);
	}

	@Override
	public void sendKeys(String text) {
		getWrappedElement().click();
		getWrappedElement().sendKeys(text);
		((PressesKey) this.getWrappedDriver()).pressKey(new KeyEvent(AndroidKey.ENTER));
		//((PressesKey) this.getWrappedDriver()).pressKey(new KeyEvent(AndroidKey.SPACE));
		//((PressesKey) this.getWrappedDriver()).pressKey(new KeyEvent(AndroidKey.DEL));
		// hideKeyboard();
		int x = this.getWrappedDriver().manage().window().getSize().getWidth() / 2;
		new TouchAction((AppiumDriver) this.getWrappedDriver()).press(PointOption.point(x, 550))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(500))).moveTo(PointOption.point(x, 300)).release()
				.perform();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void hideKeyboard() {
		// int x = this.getWrappedDriver().manage().window().getSize().getWidth() / 2;
		// new TouchAction((AppiumDriver)
		// this.getWrappedDriver()).tap(PointOption.point(x, 50)).perform();

		/*
		 * WebElement webElement = this.getWrappedDriver().findElements(By.
		 * xpath("//*[(@text and matches(@text, '[\\w\\s@]+')) or (@label and matches(@label, '[\\w\\s@]+'))]"
		 * )).get(0); System.out.println("text: " + webElement.getAttribute("text"));
		 * webElement.click();
		 */
	}
}
