package br.com.rsinet.automacao_transito_gentil.screens.widget;

import org.openqa.selenium.WebElement;

public class AndroidSettingItem extends AbstractSettingsItem {

	public AndroidSettingItem(WebElement element) {
		super(element);
	}

	@Override
	public void click() {
		this.getWrappedElement().click();
	}
}
