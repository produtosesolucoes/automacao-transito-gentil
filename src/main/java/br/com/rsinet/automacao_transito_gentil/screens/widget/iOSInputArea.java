package br.com.rsinet.automacao_transito_gentil.screens.widget;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.HasOnScreenKeyboard;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class iOSInputArea extends AbstractInputArea {

	public iOSInputArea(WebElement element) {
		super(element);
	}

	@Override
	public void sendKeys(String text) {
		getWrappedElement().sendKeys(text);
		//this.hideKeyboard();
		int x = this.getWrappedDriver().manage().window().getSize().getWidth() / 2;
		new TouchAction((AppiumDriver) this.getWrappedDriver()).press(PointOption.point(x, 450))
		.waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
		.moveTo(PointOption.point(x, 200)).release().perform();
	}

	private void hideKeyboard() {
		try {
			this.getWrappedDriver().findElement(By.xpath("////XCUIElementTypeApplication")).click();
			if (((HasOnScreenKeyboard) (this.getWrappedDriver())).isKeyboardShown()) {
				try {
					((AppiumDriver) this.getWrappedDriver()).findElementByAccessibilityId("Next").click();
				} catch (Exception e1) {
					try {
						((AppiumDriver) this.getWrappedDriver()).findElementByAccessibilityId("Done").click();
					} catch (Exception e2) {
					}
				}
			}
		} catch (Exception e) {
			try {
				((AppiumDriver) this.getWrappedDriver()).findElementByAccessibilityId("Next:").click();
			} catch (Exception e1) {
				try {
					((AppiumDriver) this.getWrappedDriver()).findElementByAccessibilityId("Done:").click();
				} catch (Exception e2) {
				}
			}
		}
	}
}
