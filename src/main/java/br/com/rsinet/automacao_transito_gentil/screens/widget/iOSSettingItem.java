package br.com.rsinet.automacao_transito_gentil.screens.widget;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class iOSSettingItem extends AbstractSettingsItem {

	public iOSSettingItem(WebElement element) {
		super(element);
	}

	@Override
	public void click() {
		Point settingsLocation = this.getWrappedElement().getLocation();
		new TouchAction((AppiumDriver) this.getWrappedDriver())
				.tap(PointOption.point(settingsLocation.x, settingsLocation.y)).perform();
	}
}
