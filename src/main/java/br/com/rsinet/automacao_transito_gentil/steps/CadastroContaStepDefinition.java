package br.com.rsinet.automacao_transito_gentil.steps;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.rsinet.automacao_transito_gentil.screens.TelaCadastro;
import br.com.rsinet.automacao_transito_gentil.screens.TelaInicial;
import br.com.rsinet.automacao_transito_gentil.screens.TelaLogin;
import br.com.rsinet.automacao_transito_gentil.screens.TelaPrincipal;
import br.com.rsinet.framework_bdd.driver.SharedDriver;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;

public class CadastroContaStepDefinition {

	private TelaInicial telaInicial;
	private TelaCadastro telaCadastro;
	private TelaPrincipal telaPrincipal;
	private TelaLogin telaLogin;
	
	@Autowired
	public CadastroContaStepDefinition(SharedDriver sharedDriver) {
		this.telaInicial = new TelaInicial(sharedDriver.getDriver());
		this.telaCadastro = new TelaCadastro(sharedDriver.getDriver());
		this.telaPrincipal = new TelaPrincipal(sharedDriver.getDriver());
		this.telaLogin = new TelaLogin(sharedDriver.getDriver());
	}

	@Dado("que estou na tela inicial")
	public void que_estou_na_tela_inicial() {
		this.telaInicial.validarTelaInicial();
	}

	@E("clico sobre Ainda não")
	public void clico_sobre_Ainda_não() {
		this.telaInicial.clicarEmRegistrar();
	}

	@E("digito o email {string}")
	public void digito_o_email(String email) {
		this.telaCadastro.digitarEmail(email);
	}

	@E("digito a senha {string}")
	public void digito_a_senha(String senha) {
		this.telaCadastro.digitarSenha(senha);
	}

	@E("clico em Cadastrar")
	public void clico_em_Cadastrar() {
		this.telaCadastro.clicarEmCadastrar();
	}

	@E("digito um cpf e data de nascimento {string}")
	public void digito_um_cpf_e_data_de_nascimento(String dataNascimento) {
		this.telaCadastro.digitarCpf();
		this.telaCadastro.digitarDataNascimento(dataNascimento);
	}

	@E("clico em Entrar e em Ok")
	public void clico_em_Entrar_e_em_Ok() {
		this.telaCadastro.clicarEmEntrar();
		this.telaCadastro.clicarEmOk();
	}

	@E("clico em Aceitar e Continuar")
	public void clico_em_Aceitar_e_Continuar() {
		this.telaCadastro.clicarAceitarContinuar();
	}

	@E("clico em Pular")
	public void clico_em_Pular() {
		this.telaCadastro.clicarEmPular();
	}

	@E("valido a pagina principal")
	public void valido_a_pagina_principal() {
		this.telaPrincipal.validarTelaPrincipal();
	}
	
	@E("faço logout")
	public void faco_logout() {
		this.telaPrincipal.fazerLogout();
	}
	
	@Entao("valido tela de login")
	public void valido_tela_login() {
		this.telaLogin.validarTelaLogin();
	}
}
