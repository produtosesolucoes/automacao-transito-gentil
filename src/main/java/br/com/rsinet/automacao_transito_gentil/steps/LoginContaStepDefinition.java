package br.com.rsinet.automacao_transito_gentil.steps;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.rsinet.automacao_transito_gentil.screens.TelaInicial;
import br.com.rsinet.automacao_transito_gentil.screens.TelaLogin;
import br.com.rsinet.framework_bdd.driver.SharedDriver;
import cucumber.api.java.pt.E;

public class LoginContaStepDefinition {

	private TelaLogin telaLogin;
	private TelaInicial telaInicial;

	@Autowired
	public LoginContaStepDefinition(SharedDriver sharedDriver) {
		this.telaInicial = new TelaInicial(sharedDriver.getDriver());
		this.telaLogin = new TelaLogin(sharedDriver.getDriver());
	}

	@E("clico sobre Sim, estou cadastrado!")
	public void clico_sobre_estou_cadastrado() {
		this.telaInicial.clicarEmFazerLogin();
	}

	@E("digito o email {string} e a senha {string}")
	public void digito_email_e_senha(String email, String senha) {
		this.telaLogin.digitarEmail(email);
		this.telaLogin.digitarSenha(senha);
	}

	@E("clico em Entrar")
	public void clico_em_entrar() {
		this.telaLogin.clicarEmLogin();
	}
}
