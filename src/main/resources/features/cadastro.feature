#language: pt
Funcionalidade: Cadastro na aplicação Transito+Gentil

@CT01_Cadastro_Conta
Esquema do Cenario: Fazer cadastro
	Dado que estou na tela inicial
	E clico sobre Ainda não
	E digito o email "<EMAIL>"
	E digito a senha "<SENHA>"
	E clico em Cadastrar
	E digito um cpf e data de nascimento "<DATA_NASCIMENTO>"
	E clico em Entrar e em Ok
	E clico em Aceitar e Continuar
	E clico em Pular
	E valido a pagina principal
	E faço logout
	Entao valido tela de login
Exemplos:
	|EMAIL|SENHA|DATA_NASCIMENTO|
	|test1_@test.com|Rsi12345|18101995|
#	|test2_@test.com|Rsi12345|18101995|
#	|test3_@test.com|Rsi12345|18101995|
#	|test4_@test.com|Rsi12345|18101995|