#language: pt
Funcionalidade: Login na aplicação Transito+Gentil

@CT02_Login_Conta
Esquema do Cenario: Fazer login
	Dado que estou na tela inicial
	E clico sobre Sim, estou cadastrado!
	E digito o email "<EMAIL>" e a senha "<SENHA>"
	E clico em Entrar
	Entao valido a pagina principal
Exemplos:
	|EMAIL|SENHA|DATA_NASCIMENTO|
	|test11@test.com|Rsi12345|18101995|